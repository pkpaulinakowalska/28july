package newnewnew;

import java.util.List;
import java.util.function.Predicate;

public class Main {

    public static void main(String[] args) {
        Predicate<Integer> podzielna = l -> l % 2 == 0;
    }

    Predicate<List<String>> listPredicate = strings -> {
        for (String tekst: strings) {
            if(tekst.contains("gotchya")) {
                return true;
            }
        }
        return false;
    };

    Predicate<List<String>> predicate = new Predicate<List<String>> {
        @Override
                public boolean test(List<String> strings){
            for (String tekst: strings) {
                if(tekst.contains("gotchya")) {
                    return true;
                }
            }
            return false;
        };

        Predicate<Person> personPredicate = p -> p.isMale();
        Predicate<Person> personPredicate1 = p -> p.getFirstName().equals("Jacek");
        Predicate<Person> personPredicate2 = p -> p.getAge() > 18;
        Predicate<List<Person>> personPredicate3 = list -> {
            for (Person p: list) {
                if(p.getAge() >=18 && p.getFirstName().equals("Jacek")){
                    return true;
                }
            }
            return false;
        };

        }
    }



