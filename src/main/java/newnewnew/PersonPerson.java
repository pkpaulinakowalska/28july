package newnewnew;

import java.util.*;
import java.util.stream.Collectors;

public class PersonPerson {

    Person person1 = new Person("Jacek","Kowalski",18,true);
    Person person2 = new Person("Jacek","Górski",15,true);
    Person person3 = new Person("Andżelika","Dżoli",25,false);
    Person person4 = new Person("Wanda","Ibanda",12,false);
    Person person5 = new Person("Marek","Marecki",17,true);
    Person person6 = new Person("Johny","Brawo",25,true);
    Person person7 = new Person("Stary","Pan",80,true);
    Person person8 = new Person("Newbie","Noob",12,true);
    Person person9 = new Person("Newbies","Sister",19,false);

    List<Person> personList = new ArrayList<>(Arrays.asList(
            person1,
            person2,
            person3,
            person4,
            person5,
            person6,
            person7,
            person8,
            person9
    ));

    //a
    List<Person> mezczyzni = personList.stream()
            .filter(p -> p.isMale())
            .collect(Collectors.toList());

    //b
    List<Person> doroslekobiety = personList.stream()
            .filter(p -> !p.isMale() && p.getAge() > 18)
            .collect(Collectors.toList());

    //c
    Optional<Person> doroslyJacek = personList.stream()
            .filter(p -> p.getFirstName().equals("Jacek"))
            .filter(p -> p.getAge() > 18)
            .findFirst();

    //d
    List<String> listaNazwisk = personList.stream()
            .filter(p -> p.getAge() > 15 && p.getAge() < 19)
            .map(p -> p.getLastName())
            .collect(Collectors.toList());

    //e
    int sum = personList.stream()
            .mapToInt(p -> p.getAge().sum());

    //f

    OptionalDouble sredniaWieku = personList.stream()
    .filter(p -> p.isMale())
    .mapToInt(p -> p.getAge()).average();
    if (sredniaWieku.isPresent()){
        System.out.println("Średnia: " + sredniaWieku.getAsDouble());
    }

    //g
    OptionalInt liczba = personList.stream()
            .sorted((o1, o2) -> {

            };
}
