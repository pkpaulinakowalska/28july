package newnewnew;

import com.sun.tools.internal.xjc.Language;

import java.util.*;
import java.util.stream.Collectors;

public class ProgrammerProgrammer extends PersonPerson {

    List<String> languages1 = Arrays.asList("Java;Cobol;Cpp;Lisp".split(";"));
    List<String> languages2 = Arrays.asList("Java;Lisp".split(";"));
    List<String> languages3 = Arrays.asList("Java;Cobol;Cpp;Lisp;C#".split(";"));
    List<String> languages4 = Arrays.asList("C#;C;Cpp".split(";"));
    List<String> languages5 = Arrays.asList("Java;Assembler;Scala;Cobol".split(";"));
    List<String> languages6 = Arrays.asList("Java;Scala".split(";"));
    List<String> languages7 = Arrays.asList("C#;C".split(";"));
    List<String> languages8 = Collections.emptyList();
    List<String> languages9 = Arrays.asList("Java");
    Programmer programmer1 = new Programmer(person1,languages1);
    Programmer programmer2 = new Programmer(person2,languages2);
    Programmer programmer3 = new Programmer(person3,languages3);
    Programmer programmer4 = new Programmer(person4,languages4);
    Programmer programmer5 = new Programmer(person5,languages5);
    Programmer programmer6 = new Programmer(person6,languages6);
    Programmer programmer7 = new Programmer(person7,languages7);
    Programmer programmer8 = new Programmer(person8,languages8);
    Programmer programmer9 = new Programmer(person9,languages9);
    List<Programmer> programmers = Arrays.asList(programmer1,programmer2,programmer3,programmer4,
            programmer5,programmer6,programmer7,programmer8,programmer9);


    List<Programmer> mezczyzni = programmers.stream()
            .filter(p -> p.getPerson().isMale() )
            .collect(Collectors.toList());

    List<Programmer> niepełnoletniCabol = programmers.stream()
            .filter(p -> p.getLanguages()  )
            .filter(p -> p.getPerson().getAge() < 18)
            .collect(Collectors.toList());

    List<Programmer> mezczyzniKilkaJezykow = programmers.stream()
            .filter(p -> p.getPerson().isMale())
            .filter(p -> p.getLanguages() )
            .collect(Collectors.toList());

    List<Programmer> kobietyJavaCpp = programmers.stream()
            .filter(p -> !p.getPerson().isMale())


    List<String> meskieImiona = programmers.stream()
            .filter(p -> p.getPerson().isMale())
            .map(p -> p.getPerson().getFirstName())
            .collect(Collectors.toList());

    Set<String> jezyki = programmers.stream()
            .map(p -> p.getLanguages())
            .flatMap(Collection::stream)
            .collect(Collectors.toSet());



}
